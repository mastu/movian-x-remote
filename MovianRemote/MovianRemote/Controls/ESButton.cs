﻿using System;
using Xamarin.Forms;

namespace MovianRemote
{
	public class ESButton : Button
	{
		public static BindableProperty ImageSourceProperty = BindableProperty.Create ("ImageSource", typeof(ImageSource), typeof(ESButton), null);
		public static BindableProperty ImageFrameWidthProperty = BindableProperty.Create ("ImageFrameWidth", typeof(int), typeof(ESButton), 0);
		public static BindableProperty ImageFrameColorProperty = BindableProperty.Create ("ImageFrameColor", typeof(Color), typeof(ESButton), Color.Transparent);
		public static BindableProperty ImageKeepAspectRatioProperty = BindableProperty.Create ("ImageKeepAspectRatio", typeof(bool), typeof(ESButton), true);


		[TypeConverter (typeof(ImageSourceConverter))] 
		public ImageSource ImageSource {
			get { 
				return (ImageSource)GetValue (ImageSourceProperty);
			}
			set {
				SetValue (ImageSourceProperty, value);
			}
		}

		public int ImageFrameWidth {
			get {
				return (int)GetValue (ImageFrameWidthProperty);
			}
			set {
				SetValue (ImageFrameWidthProperty, value);
			}
		}

		public Color ImageFrameColor {
			get {
				return (Color)GetValue (ImageFrameColorProperty);
			}
			set {
				SetValue (ImageFrameColorProperty, value);
			}
		}

		public bool ImageKeepAspectRatio {
			get {
				return (bool)GetValue (ImageKeepAspectRatioProperty);
			}
			set {
				SetValue (ImageKeepAspectRatioProperty, value);
			}
		}

	}
}

