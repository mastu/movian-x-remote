﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
//using Refractored.Xam.Settings;

namespace MovianRemote
{
	public partial class App : Application
	{
		public App ()
		{		
			InitializeComponent ();
			//MainPage = new FirstPage (new FirstViewModel ());
			MainPage = new MenuPage ();
		}

		protected override void OnStart ()
		{
			AppModel.Instance.ReadSettings ();
			MessagingCenter.Send (this, "OnStart");
		}

		protected override void OnSleep ()
		{
			AppModel.Instance.SaveSettings ();
		}

		protected override void OnResume ()
		{
			AppModel.Instance.ReadSettings ();
			MessagingCenter.Send (this, "OnResume");
		}
	}
}

