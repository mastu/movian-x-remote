﻿using System;

namespace MovianRemote
{
	public class Message
	{
		public string Title{ get; set; }

		public string Body { get; set; }

		public object Parameter { get; set; }
	}
}

