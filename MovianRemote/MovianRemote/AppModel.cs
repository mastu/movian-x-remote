﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
//using Refractored.Xam.Settings;
using Plugin.Settings;

namespace MovianRemote
{
	class AppModel
	{

		//private const string movianIP = "192.168.0.234";
		//private const string movianIP = "movian.home.lan";
		private readonly string apiAction = @"http://{0}:42000/showtime/input/action/{1}";

		public string Host { get; set;}
		public bool IsVibrate { get; set;}
	
		private static AppModel instance;

		public static AppModel Instance {
			get {
				if (instance == null) {
					instance = new AppModel ();

				}
				return instance;
			}
		}


		public async Task<bool> SendAction (string action)
		{
			using (var httpClient = new HttpClient ()) {
				var url = string.Format (apiAction, Host, action);
				var response = await httpClient.GetAsync (url);
				return response.IsSuccessStatusCode;
			}
		}

		public void SaveSettings()
		{
			CrossSettings.Current.AddOrUpdateValue ("Host", Host);
			CrossSettings.Current.AddOrUpdateValue ("Vibrate", IsVibrate);
		}

		public void ReadSettings()
		{
		    Host = "Test Host"; // CrossSettings.Current.GetValueOrDefault ("Host", "movian.local");
			IsVibrate = CrossSettings.Current.GetValueOrDefault ("Vibrate", false);
		}
		
	}
}

