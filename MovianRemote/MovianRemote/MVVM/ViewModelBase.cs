﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq.Expressions;
using Prism.Mvvm;

namespace MovianRemote
{
	public abstract class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		#region IViewModel implementation

		public void SetState<T> (Action<T> action) where T : class
		{
			action (this as T);
		}

		public string Title { get ; set; }


		#endregion

		protected virtual bool SetProperty<T> (ref T storage, T value, [CallerMemberName] string propertyName = null)
		{
		
			if (object.Equals (storage, value))
				return false;

			storage = value;
			OnPropertyChanged (propertyName);
			return true;
		}

		protected void OnPropertyChanged ([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler.Invoke (this, new PropertyChangedEventArgs (propertyName));
		}


		protected  void OnPropertyChanged<T> (Expression<Func<T>> propertyExpression)
		{
			var propertyName = PropertySupport.ExtractPropertyName (propertyExpression);
			OnPropertyChanged (propertyName);
		}

	}
}

