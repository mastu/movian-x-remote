﻿using System;
using Xamarin.Forms;

namespace MovianRemote
{
	public class MenuViewModel : ViewModelBase
	{
		public MenuViewModel ()
		{
			MessagingCenter.Subscribe<App> (this, "OnStart", updateSettings);
			MessagingCenter.Subscribe<App> (this, "OnResume", updateSettings);
		}

		//private string host;
		public string Host {
			get {
				return AppModel.Instance.Host;
			}
			set {
				if (AppModel.Instance.Host == value)
					return;
				AppModel.Instance.Host = value;
				OnPropertyChanged (() => Host);
			}
		}


		public bool IsVibrate {
			get {
				return AppModel.Instance.IsVibrate;
			}
			set {
				if (AppModel.Instance.IsVibrate == value)
					return;
				AppModel.Instance.IsVibrate = value;
				OnPropertyChanged (() => IsVibrate);
			}
		}


		private void updateSettings (object sender)
		{
			OnPropertyChanged (() => IsVibrate);
			OnPropertyChanged ("Host");
		}
	}
}

