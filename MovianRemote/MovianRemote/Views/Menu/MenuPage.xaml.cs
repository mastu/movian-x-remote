﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MovianRemote
{
	public partial class MenuPage : MasterDetailPage
	{
		public MenuPage ()
		{

			NavigationPage.SetHasNavigationBar (this, false);
			InitializeComponent ();

			BindingContext = new { 
				Master = new MenuViewModel (),
				Detail = new MoveViewModel ()};

			MessagingCenter.Subscribe<MoveViewModel> (this, "ShowMenuToggle", (sender) => ShowMenuToggle ()); 
			MessagingCenter.Subscribe<MoveViewModel> (this, "DisplayMessage", DisplayMessage);
	
		}

		private void DisplayMessage (MoveViewModel sender)
		{
			DisplayAlert ("Error", string.Format ("Can't connect to Movian {0} \n Check Name or IP address", AppModel.Instance.Host), "Dismiss");
		}

		public void ShowMenuToggle ()
		{
			IsPresented = !IsPresented;
		}
	}
}

