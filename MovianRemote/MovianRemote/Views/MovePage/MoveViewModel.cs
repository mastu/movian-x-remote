﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;
using Plugin.Vibrate;

namespace MovianRemote
{
	public class MoveViewModel : ViewModelBase
	{
		

		public ICommand ShowMenuCommand {
			get {
				return new Command (() => MessagingCenter.Send<MoveViewModel> (this, "ShowMenuToggle"));
			}
		}

		private Command remoteCommand;

		public ICommand RemoteCommand {
			get { 
				return remoteCommand = remoteCommand ?? new Command (async (commandAction) => {
					#if DEBUG
					Debug.WriteLine (commandAction);
				
					#endif
					try {
						await AppModel.Instance.SendAction ((string)commandAction);
						if (AppModel.Instance.IsVibrate)
							CrossVibrate.Current.Vibration (50);
						
					} catch {
						displayMessage ();
					}
				});
			}
		}


		private void displayMessage ()
		{
			MessagingCenter.Send<MoveViewModel> (this, "DisplayMessage");
		}



	}


}

