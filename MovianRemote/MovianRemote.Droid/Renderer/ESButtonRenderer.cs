﻿using System;
using Xamarin.Forms;
using MovianRemote;
using MovianRemote.Droid;
using Xamarin.Forms.Platform.Android.AppCompat;
using System.Threading.Tasks;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Android.Graphics.Drawables;

[assembly: ExportRenderer(typeof(ESButton), typeof(ESButtonRenderer))]
namespace MovianRemote.Droid
{
	public class ESButtonRenderer : Xamarin.Forms.Platform.Android.ButtonRenderer
	{
		protected async override void OnElementChanged (Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			if (e.OldElement != null) {
			}

			if (e.NewElement == null)
				return;

		

			var button = (ESButton)Element;

			if (button == null || button.ImageSource == null)
				return;

			await SetImageAsync (button);
		
		}


		public override bool OnTouchEvent (Android.Views.MotionEvent e)
		{
			if (e.Action == Android.Views.MotionEventActions.ButtonPress) {
				
			}

			return base.OnTouchEvent (e);
		}

		protected async override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			var button = (ESButton)Element;
			if (button == null)
				return;

			if (e.PropertyName == "ImageSource" || e.PropertyName == "ImageFrameWidth" || e.PropertyName == "ImageKeepAspectRatio" || e.PropertyName == "WidthRequest" || e.PropertyName == "HeightRequest") 
				await SetImageAsync (button);
		}

		private async Task SetImageAsync(ESButton button)
		{

			using(var bitmap= await GetBitmapAsync(button.ImageSource))
			{
				if (bitmap == null || bitmap.Width <= 0 || bitmap.Height <= 0)
					return;
				
				var drawable = new BitmapDrawable (bitmap); 
				var scaledDrawable = GetScaleDrawable (drawable,(int)(button.Width),(int)(button.Height));
				Control.Background = scaledDrawable;
				
			}

		}

//		public int RequestToPixels(int sizeRequest)
//		{
//			return (int)(sizeRequest); //* Resources.DisplayMetrics.Density);
//		}

		private async Task<Bitmap> GetBitmapAsync(ImageSource source)
		{
			var handler = GetHandler (source);
			var returnValue = (Bitmap)null;
		
			if (handler != null)
				returnValue = await handler.LoadImageAsync (source, this.Context);
		
			return returnValue;
		}


		private static IImageSourceHandler GetHandler (ImageSource source)
		{
			IImageSourceHandler returnValue = null;
			if (source is UriImageSource) {
				returnValue = new ImageLoaderSourceHandler ();
			} else if (source is FileImageSource) {
				returnValue = new FileImageSourceHandler ();
			} else if (source is StreamImageSource) {
				returnValue = new StreamImagesourceHandler ();
			}
			return returnValue;
		}

		private Drawable GetScaleDrawable(Drawable drawable, int width, int height)
				{
					var returnValue = new ScaleDrawable(drawable, 0, width, height).Drawable;
					returnValue.SetBounds(0, 0, width, height);
					return returnValue;
				}
	}
}
