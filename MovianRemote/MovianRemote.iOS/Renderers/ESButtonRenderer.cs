﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using MovianRemote;
using MovianRemote.iOS;
using System.Threading.Tasks;
using UIKit;
using System.Drawing;
using System.Diagnostics;

[assembly: ExportRenderer (typeof(ESButton), typeof(ESButtonRenderer))]
namespace MovianRemote.iOS
{
	
	public class ESButtonRenderer : ButtonRenderer
	{
		protected async override void OnElementChanged (ElementChangedEventArgs<Button> e)
		{

			base.OnElementChanged (e);

			if (Control == null)
				return;

			if (e.NewElement == null)
				return;

			var button = (ESButton)Element;

			await SetImageAsync (button);
		}

		protected async override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			var button = (ESButton)Element;
			if (button == null)
				return;

			if (e.PropertyName == "ImageSource" || e.PropertyName == "ImageFrameWidth" || e.PropertyName == "ImageKeepAspectRatio" || e.PropertyName == "WidthRequest" || e.PropertyName == "HeightRequest") {
				await SetImageAsync (button);
			}
		}

		private static IImageSourceHandler GetHandler (ImageSource source)
		{
			IImageSourceHandler returnValue = null;
			if (source is UriImageSource) {
				returnValue = new ImageLoaderSourceHandler ();
			} else if (source is FileImageSource) {
				returnValue = new FileImageSourceHandler ();
			} else if (source is StreamImageSource) {
				returnValue = new StreamImagesourceHandler ();
			}
			return returnValue;
		}

		private async  Task SetImageAsync (ESButton button)
		{
			if (button == null)
				return;

			var imageHandler = GetHandler (button.ImageSource);
			if (imageHandler == null)
				return;

			using (var image = await imageHandler.LoadImageAsync (button.ImageSource)) {
				if (image == null || image.Size.Height <= 0 || image.Size.Width <= 0)
					return;
				if (button.ImageKeepAspectRatio) {
					var resutImage =  button.ImageKeepAspectRatio ? getScaledImage (button, image) : getImage(button,image);
					using( var resizibleImage = resutImage.CreateResizableImage(new UIEdgeInsets(0,0,(nfloat)button.Width,(nfloat)button.Height))){

						Control.SetImage (resizibleImage.ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal), UIControlState.Normal);						
					}
				}
			}
		}

		private UIImage getScaledImage (ESButton button,UIImage image)
		{
			var width = button.WidthRequest;
			var height = button.HeightRequest;
			double newImageWidth;
			double newImageHeight;
			#if DEBUG
			Debug.Write(image.Size);
			#endif
			var scale = Math.Min (width / image.Size.Width, height / image.Size.Height);

			//scale = scale < 1 ? scale : Math.Min (width / image.Size.Width, height / image.Size.Height);

			newImageHeight = scale * image.Size.Height - button.ImageFrameWidth*2;
			newImageWidth = scale * image.Size.Width - button.ImageFrameWidth*2;
			//var backgroundColor = button.BackgroundColor;
			UIGraphics.BeginImageContext (new SizeF ((float)width,(float) height));
			if (button.ImageFrameWidth > 0) {
				var context = UIGraphics.GetCurrentContext ();
				context.SetFillColor (button.ImageFrameColor.ToCGColor ());
				context.FillRect (new RectangleF (0, 0, (float)width,(float) height));
				context.SetFillColor (button.BackgroundColor.ToCGColor ());
				context.FillRect (new RectangleF (button.ImageFrameWidth, button.ImageFrameWidth, (float)width-button.ImageFrameWidth*2,(float) height-button.ImageFrameWidth*2));
			
			}
			image.Draw (new RectangleF (
				(float)(width - newImageWidth) / 2 , 
				(float)(height - newImageHeight) / 2 , 
				(float)newImageWidth, 
				(float)newImageHeight));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();
			return resultImage;

		}

		private UIImage getImage(ESButton button,UIImage image){
			var width = button.WidthRequest;
			var height = button.HeightRequest;
			UIGraphics.BeginImageContext (new SizeF ((float)width,(float) height));
			image.Draw (new RectangleF (
				(float)0 + button.ImageFrameWidth, 
				(float)0 + button.ImageFrameWidth, 
				(float)width- button.ImageFrameWidth*2, 
				(float)height-button.ImageFrameWidth*2));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();
			return resultImage;
		}
	}


}

