﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Microsoft.Phone.Scheduler;
using MovianRemote;
using MovianRemote.WinPhone.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;
using ImageSource = Xamarin.Forms.ImageSource;


[assembly: ExportRenderer(typeof(ESButton), typeof(ESButtonRenderer))]
namespace MovianRemote.WinPhone.Renderers
{
    public class ESButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
                return;

            if(e.NewElement == null)
                return;

            var button = (ESButton) Element;

            SetImage(button);
        }




        private async void SetImage(ESButton button)
        {
            if(button.ImageSource == null)
                return;

            var imageSource = await GetImageSourceAsync(button.ImageSource);

            Control.BorderThickness = new System.Windows.Thickness(0);           
            Control.Background = new ImageBrush { ImageSource = imageSource};
            


        }


        private async Task<System.Windows.Media.ImageSource> GetImageSourceAsync(ImageSource source)
        {
          
            var handler = GetHandler(source);

            if (source is FileImageSource)
            {
                var fileSource = (FileImageSource) source;
                fileSource.File = "Images/" + fileSource.File;
            }

            var returnValue = (System.Windows.Media.ImageSource)null;

            if (handler != null)
                returnValue = await handler.LoadImageAsync(source);

            return returnValue;
        }

        private static IImageSourceHandler GetHandler(ImageSource source)
        {
            IImageSourceHandler returnValue = null;
            if (source is UriImageSource)
            {
                returnValue = new ImageLoaderSourceHandler();
            }
            else if (source is FileImageSource)
            {
                returnValue = new FileImageSourceHandler();
            }
            else if (source is StreamImageSource)
            {
                returnValue = new StreamImagesourceHandler();
            }
            return returnValue;
        }
    }
}
